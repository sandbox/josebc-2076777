<?php

class FacetapiDatepicker extends FacetapiWidget {

  public function __construct($id, array $realm, FacetapiFacet $facet, stdClass $settings) {
    parent::__construct($id, $realm, $facet, $settings);
    $this->key = $facet['name'];
  }

  /**
   * Renders the form.
   */
  public function execute() {
    $element = &$this->build[$this->facet['field alias']];
    $name = array_pop(explode(':', $this->build['#settings']->name));
    $element = drupal_get_form('facetapi_datepicker_' .  $name, $element, $this->build['#facet']['field']);
  }
}


/**
 * Generate form for facet.
 */
function facetapi_datepicker($form, &$form_state, $elements, $field) {

  $form['start'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-5:+0',
  );

  $form['end'] = array(
    '#type' => 'date_popup',
    '#title' => t('To'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-5:+0',
  );

  $form['field'] = array(
    '#type' => 'hidden',
    '#value' => $field,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#submit' => array('facetapi_datepicker_submit'),
  );

  // Add javascript that hides Filter button.
  $form['#attached']['js'][] = drupal_get_path('module', 'facetapi_datepicker') . '/js/facetapi-datepicker.js';

  $form['#attributes']['class'][] = 'facetapi-datepicker';

  return $form;
}

/**
 * Submit handler for facet form.
 */
function facetapi_datepicker_submit($form, &$form_state) {

  $iso_start = facetapi_isodate(strtotime($form_state['values']['start']));
  $iso_end   = facetapi_isodate(strtotime($form_state['values']['end']));
  $field     = $form_state['values']['field'];

  // TODO: find a better was for handling the redirect arguments.
  if(!empty($_GET['f'])) {
    foreach ($_GET['f'] as $key => $part) {
      if(strpos($part, $field) === 0) {
        unset($_GET['f'][$key]);
      }
    }
  }
  $query = array('f' => $_GET['f']);
  $query['f'][] = $field . ':[' . $iso_start . ' TO ' . $iso_end . ']';
  unset($query[q]);
  $form_state['redirect'] = array($_GET['q'], array('query' => $query));
}
